class Letters {
  constructor(letterGrid) {
    this.letterGrid = letterGrid;
  }

  findWords(words) {
    const size = this.letterGrid.length;
    let result = {};

    for (let word of words) {
      let wordResult;
      const wordRevesed = word.split("").reverse().join("");
      
      // rows
      for (let i = 0; i < size; i++) {
        const start = this.letterGrid[i].indexOf(word);
        if (start !== -1) {
          wordResult = {
            start: [i + 1, start + 1],
            end: [i + 1, start + word.length],
          };
          break;
        }
        const reversedStart = this.letterGrid[i].indexOf(wordRevesed);
        if (reversedStart !== -1) {
          wordResult = {
            start: [i + 1, reversedStart + word.length],
            end: [i + 1, reversedStart + 1],
          };
        }
      }

      // columns
      for (let i = 0; i < size; i++) {
        const column = this.letterGrid.map((row) => row[i]).join("");
        const start = column.indexOf(word);
        if (start !== -1) {
          wordResult = {
            start: [start + 1, i + 1],
            end: [start + word.length, i + 1],
          };
          break;
        }
        const reversedStart = column.indexOf(wordRevesed);
        if (reversedStart !== -1) {
          wordResult = {
            start: [reversedStart + word.length, i + 1],
            end: [reversedStart + 1, i + 1],
          };
          break;
        }
      }

      if (!result[word]) {
        // first diagonal
        let firstDiagonal = "";
        for (let i = 0; i < size; i++) {
          firstDiagonal += this.letterGrid[i][i];
        }
        const firstDiagonalStart = firstDiagonal.indexOf(word);
        if (firstDiagonalStart !== -1) {
          wordResult = {
            start: [firstDiagonalStart + 1, firstDiagonalStart + 1],
            end: [
              firstDiagonalStart + word.length,
              firstDiagonalStart + word.length,
            ],
          };
        }
        const firstDiagonalReversedStart = firstDiagonal.indexOf(wordRevesed);
        if (firstDiagonalReversedStart !== -1) {
          wordResult = {
            start: [
              firstDiagonalReversedStart + word.length,
              firstDiagonalReversedStart + word.length,
            ],
            end: [firstDiagonalReversedStart + 1, firstDiagonalReversedStart + 1],
          };
        }
      }

      if (!result[word]) {
        // second diagonal
        let secondDiagonal = "";
        for (let i = 0, j = size - 1; i < size; i++) {
          secondDiagonal += this.letterGrid[j][i];
          j--;
        }
        const secondDiagonalStart = secondDiagonal.indexOf(word);
        if (secondDiagonalStart !== -1) {
          wordResult = {
            start: [size - secondDiagonalStart, secondDiagonalStart + 1],
            end: [
              size - secondDiagonalStart - word.length + 1,
              secondDiagonalStart + word.length,
            ],
          };
        }
        const secondDiagonalReversedStart = secondDiagonal.indexOf(wordRevesed);
        if (secondDiagonalReversedStart !== -1) {
          wordResult = {
            start: [
              secondDiagonalReversedStart + 1,
              size - secondDiagonalReversedStart,
            ],
            end: [
              secondDiagonalReversedStart + word.length,
              size - (secondDiagonalStart + 1) - word.length,
            ],
          };
        }
      }

      result[word] = wordResult;
    }

    return result;
  }
}

module.exports = Letters;
