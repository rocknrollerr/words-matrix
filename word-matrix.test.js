const Letters = require("./index");

const letters = new Letters([
  "oguitar",
  "o____e_",
  "p_j_c_b",
  "s__a__a",
  "__p_z_s",
  "_s___zs",
  "_ram___",
]);

test("row", () => {
  expect(letters.findWords(["ram"])).toEqual({
    ram: { start: [7, 2], end: [7, 4] },
  });
});

test("row reversed", () => {
  expect(letters.findWords(["mar"])).toEqual({
    mar: { start: [7, 4], end: [7, 2] },
  });
});

test("column", () => {
  expect(letters.findWords(["oops"])).toEqual({
    oops: { start: [1, 1], end: [4, 1] },
  });
});

test("column reversed", () => {
  expect(letters.findWords(["spoo"])).toEqual({
    spoo: { start: [4, 1], end: [1, 1] },
  });
});

test("first diagonal", () => {
  expect(letters.findWords(["jazz"])).toEqual({
    jazz: { start: [3, 3], end: [6, 6] },
  });
});

test("first diagonal reversed", () => {
  expect(letters.findWords(["zzaj"])).toEqual({
    zzaj: { start: [6, 6], end: [3, 3] },
  });
});

test("second diagonal", () => {
  expect(letters.findWords(["space"])).toEqual({
    space: { start: [6, 2], end: [2, 6] },
  });
});

test("second diagonal reversed", () => {
  expect(letters.findWords(["ecaps"])).toEqual({
    ecaps: { start: [2, 6], end: [6, 2] },
  });
});

test("multiple words", () => {
  expect(letters.findWords(["guitar", "bass"])).toEqual({
    guitar: { start: [1, 2], end: [1, 7] },
    bass: { start: [3, 7], end: [6, 7] },
  });
});
